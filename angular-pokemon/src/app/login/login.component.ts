import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrl: './login.component.scss'
})
export class LoginComponent implements OnInit {
message: string = 'Vous etes Deconnecté. ( Afpa / Afpa )';
name:string;
password:string;
auth:AuthService;
  
  constructor(private authService: AuthService,private router : Router) {}

  ngOnInit() {
    this.auth = this.authService;
  }

setMessage() {
if (this.authService.isLoggedIn) {
  this.message = 'Vous etes Connecté.';
}
else{
  this.message = 'Identifant ou mot de passe incorrect.';
}

}


login() {
  this.message = 'Tentative de connexion en cours...';
  this.authService.login(this.name, this.password)
  .subscribe((isLoggedIn: boolean) => {
    this.setMessage();
    if (isLoggedIn) {
    this.router.navigate(['/pokemons']);
    }else{
      this.password = '';
      this.router.navigate(['/login']);
    }
})  
}

  // Déconnecte l'utilisateur
  logout() {
    this.authService.logout();
    this.setMessage();
}


}
