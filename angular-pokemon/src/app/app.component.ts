import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PokemonService } from './pokemon/pokemon.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})


export class AppComponent {


  constructor(
       private router : Router
    ){}

    goToPokemonList(){
      this.router.navigate(['/pokemons']);
  
    }

}
