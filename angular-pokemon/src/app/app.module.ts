import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { DirectiveModeleDirective } from './pokemon/directives/directive-modele.directive';
import { PokemonModule } from './pokemon/pokemon.module';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpClientInMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from './in-memory-data.service';
import { LoginComponent } from './login/login.component';
 


@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    HttpClientInMemoryWebApiModule.forRoot(InMemoryDataService,{dataEncapsulation: false}),  
    PokemonModule,
    AppRoutingModule,
    HttpClientModule
    ],
    
  declarations: [
    AppComponent,
    PageNotFoundComponent,
    DirectiveModeleDirective,
    LoginComponent,

  
  ],
  providers: [  ],
  bootstrap: [    AppComponent  ]
})
export class AppModule { }
