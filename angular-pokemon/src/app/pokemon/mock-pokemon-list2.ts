import { Pokemon } from './pokemon';

export const POKEMONS: Pokemon[] = [
    {
        id: 1,
        name: "Bulbizarre",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/1.gif",
        types: ["Plante", "Poison"],
        created: new Date()
    },{
        id: 2,
        name: "Herbizarre",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/2.gif",
        types: ["Plante", "Poison"],
        created: new Date()
    },{
        id: 3,
        name: "Florizarre",
        hp: 35,
        cp: 8,
        picture: "https://www.shinyhunters.com/images/regular/3.gif",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 4,
        name: "Salamèche",
        hp: 28,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/4.gif",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 5,
        name: "Reptincel",
        hp: 32,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/5.gif",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 6,
        name: "Dracaufeu",
        hp: 38,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/6.gif",
        types: ["Feu", "Vol"],
        created: new Date()
    },
    {
        id: 7,
        name: "Carapuce",
        hp: 21,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/7.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 8,
        name: "Carabaffe",
        hp: 21,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/8.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 9,
        name: "Tortank",
        hp: 21,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/9.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 10,
        name: "Chenipan",
        hp: 16,
        cp: 2,
        picture: "https://www.shinyhunters.com/images/regular/10.gif",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 11,
        name: "Chrysacier",
        hp: 30,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/11.gif",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 12,
        name: "Papilusion",
        hp: 18,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/12.gif",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 13,
        name: "Aspicot",
        hp: 14,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/13.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 14,
        name: "Coconfort",
        hp: 16,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/14.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 15,
        name: "Dardargnan",
        hp: 21,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/15.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 16,
        name: "Roucool",
        hp: 19,
        cp: 3,
        picture: "https://www.shinyhunters.com/images/regular/16.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 17,
        name: "Roucoups",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/17.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 18,
        name: "Roucarnage",
        hp: 17,
        cp: 8,
        picture: "https://www.shinyhunters.com/images/regular/18.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    
    {
        id: 19,
        name: "Rattata",
        hp: 10,
        cp: 0,
        picture: "https://www.shinyhunters.com/images/regular/19.gif",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 20,
        name: "Rattatac",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/20.gif",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 21,
        name: "Piafabec",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/21.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 22,
        name: "Rapasdepic",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/22.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 23,
        name: "Abo",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/23.gif",
        types: ["Poison"],
        created: new Date()
    },{
        id: 24,
        name: "Arbok",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/24.gif",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 25,
        name: "Pikachu",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/25.gif",
        types: ["Electrik"],
        created: new Date()
    },{
        id: 26,
        name: "Raichu",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/26.gif",
        types: ["Electrik"],
        created: new Date()
    },
    {
        id: 27,
        name: "Sabelette",
        hp: 22,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/27.gif",
        types: ["Sol"],
        created: new Date()
    },{
        id: 28,
        name: "Sablaireau",
        hp: 30,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/28.gif",
        types: ["Sol"],
        created: new Date()
    },{
        id: 29,
        name: "Nidoran♀",
        hp: 24,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/29.gif",
        types: ["Poison"],
        created: new Date()
    },{
        id: 30,
        name: "Nidorina",
        hp: 28,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/30.gif",
        types: ["Poison"],
        created: new Date()
    },{
        id: 31,
        name: "Nidoqueen",
        hp: 35,
        cp: 8,
        picture: "https://www.shinyhunters.com/images/regular/31.gif",
        types: ["Poison", "Sol"],
        created: new Date()
    },{
        id: 32,
        name: "Nidoran♂",
        hp: 24,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/32.gif",
        types: ["Poison"],
        created: new Date()
    },{
        id: 33,
        name: "Nidorino",
        hp: 28,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/33.gif",
        types: ["Poison"],
        created: new Date()
    },{
        id: 34,
        name: "Nidoking",
        hp: 35,
        cp: 8,
        picture: "https://www.shinyhunters.com/images/regular/34.gif",
        types: ["Poison", "Sol"],
        created: new Date()
    },{
        id: 35,
        name: "Mélofée",
        hp: 25,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/35.gif",
        types: ["Fée"],
        created: new Date()
    },{
        id: 36,
        name: "Mélodelfe",
        hp: 30,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/36.gif",
        types: ["Fée"],
        created: new Date()
    },{
        id: 37,
        name: "Goupix",
        hp: 24,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/37.gif",
        types: ["Feu"],
        created: new Date()
    },{
        id: 38,
        name: "Feunard",
        hp: 28,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/38.gif",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 39,
        name: "Rondoudou",
        hp: 24,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/39.gif",
        types: ["Normal", "Fée"],
        created: new Date()
    },
    {
        id: 40,
        name: "Grodoudou",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/40.gif",
        types: ["Normal", "Fée"],
        created: new Date()
    },
    {
        id: 41,
        name: "Nosferapti",
        hp: 22,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/41.gif",
        types: ["Poison", "Vol"],
        created: new Date()
    },
    {
        id: 42,
        name: "Nosferalto",
        hp: 28,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/42.gif",
        types: ["Poison", "Vol"],
        created: new Date()
    },
    {
        id: 43,
        name: "Mystherbe",
        hp: 20,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/43.gif",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 44,
        name: "Ortide",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/44.gif",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 45,
        name: "Rafflesia",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/45.gif",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 46,
        name: "Paras",
        hp: 20,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/46.gif",
        types: ["Insecte", "Plante"],
        created: new Date()
    },
    {
        id: 47,
        name: "Parasect",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/47.gif",
        types: ["Insecte", "Plante"],
        created: new Date()
    },{
        id: 48,
        name: "Mimitoss",
        hp: 22,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/48.gif",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 49,
        name: "Aéromite",
        hp: 28,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/49.gif",
        types: ["Insecte", "Poison"],
        created: new Date()
    },
    {
        id: 50,
        name: "Taupiqueur",
        hp: 20,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/50.gif",
        types: ["Sol"],
        created: new Date()
    },
    {
        id: 51,
        name: "Triopikeur",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/51.gif",
        types: ["Sol"],
        created: new Date()
    },
    {
        id: 52,
        name: "Miaouss",
        hp: 22,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/52.gif",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 53,
        name: "Persian",
        hp: 28,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/53.gif",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 54,
        name: "Psykokwak",
        hp: 22,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/54.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 55,
        name: "Akwakwak",
        hp: 28,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/55.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 56,
        name: "Férosinge",
        hp: 22,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/56.gif",
        types: ["Combat"],
        created: new Date()
    },{
        id: 57,
        name: "Colossinge",
        hp: 28,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/57.gif",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 58,
        name: "Caninos",
        hp: 24,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/58.gif",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 59,
        name: "Arcanin",
        hp: 32,
        cp: 8,
        picture: "https://www.shinyhunters.com/images/regular/59.gif",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 60,
        name: "Ptitard",
        hp: 18,
        cp: 3,
        picture: "https://www.shinyhunters.com/images/regular/60.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 61,
        name: "Têtarte",
        hp: 24,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/61.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 62,
        name: "Tartard",
        hp: 30,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/62.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 63,
        name: "Abra",
        hp: 16,
        cp: 3,
        picture: "https://www.shinyhunters.com/images/regular/63.gif",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 64,
        name: "Kadabra",
        hp: 22,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/64.gif",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 65,
        name: "Alakazam",
        hp: 28,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/65.gif",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 66,
        name: "Machoc",
        hp: 28,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/66.gif",
        types: ["Combat"],
        created: new Date()
    },{
        id: 67,
        name: "Machopeur",
        hp: 32,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/67.gif",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 68,
        name: "Mackogneur",
        hp: 35,
        cp: 8,
        picture: "https://www.shinyhunters.com/images/regular/68.gif",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 69,
        name: "Chétiflor",
        hp: 20,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/69.gif",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 70,
        name: "Boustiflor",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/70.gif",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 71,
        name: "Empiflor",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/71.gif",
        types: ["Plante", "Poison"],
        created: new Date()
    },
    {
        id: 72,
        name: "Tentacool",
        hp: 22,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/72.gif",
        types: ["Eau", "Poison"],
        created: new Date()
    },
    {
        id: 73,
        name: "Tentacruel",
        hp: 30,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/73.gif",
        types: ["Eau", "Poison"],
        created: new Date()
    },
    {
        id: 74,
        name: "Racaillou",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/74.gif",
        types: ["Roche", "Sol"],
        created: new Date()
    },
    {
        id: 75,
        name: "Gravalanch",
        hp: 35,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/75.gif",
        types: ["Roche", "Sol"],
        created: new Date()
    },
    {
        id: 76,
        name: "Grolem",
        hp: 40,
        cp: 8,
        picture: "https://www.shinyhunters.com/images/regular/76.gif",
        types: ["Roche", "Sol"],
        created: new Date()
    }, {
        id: 77,
        name: "Ponyta",
        hp: 24,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/77.gif",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 78,
        name: "Galopa",
        hp: 28,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/78.gif",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 79,
        name: "Ramoloss",
        hp: 30,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/79.gif",
        types: ["Eau", "Psy"],
        created: new Date()
    },
    {
        id: 80,
        name: "Flagadoss",
        hp: 35,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/80.gif",
        types: ["Eau", "Psy"],
        created: new Date()
    },
    {
        id: 81,
        name: "Magnéti",
        hp: 20,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/81.gif",
        types: ["Électrique", "Acier"],
        created: new Date()
    },
    {
        id: 82,
        name: "Magnéton",
        hp: 25,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/82.gif",
        types: ["Électrique", "Acier"],
        created: new Date()
    },
    {
        id: 83,
        name: "Canarticho",
        hp: 25,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/83.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 84,
        name: "Doduo",
        hp: 20,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/84.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 85,
        name: "Dodrio",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/85.gif",
        types: ["Normal", "Vol"],
        created: new Date()
    },
    {
        id: 86,
        name: "Otaria",
        hp: 25,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/86.gif",
        types: ["Eau"],
        created: new Date()
    },{
        id: 87,
        name: "Lamantine",
        hp: 35,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/87.gif",
        types: ["Eau", "Glace"],
        created: new Date()
    },
    {
        id: 88,
        name: "Tadmorv",
        hp: 30,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/88.gif",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 89,
        name: "Grotadmorv",
        hp: 35,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/89.gif",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 90,
        name: "Kokiyas",
        hp: 20,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/90.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 91,
        name: "Crustabri",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/91.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 92,
        name: "Fantominus",
        hp: 18,
        cp: 3,
        picture: "https://www.shinyhunters.com/images/regular/92.gif",
        types: ["Spectre", "Poison"],
        created: new Date()
    },
    {
        id: 93,
        name: "Spectrum",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/93.gif",
        types: ["Spectre", "Poison"],
        created: new Date()
    },
    {
        id: 94,
        name: "Ectoplasma",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/94.gif",
        types: ["Spectre", "Poison"],
        created: new Date()
    },
    {
        id: 95,
        name: "Onix",
        hp: 35,
        cp: 8,
        picture: "https://www.shinyhunters.com/images/regular/95.gif",
        types: ["Roche", "Sol"],
        created: new Date()
    },
    {
        id: 96,
        name: "Soporifik",
        hp: 20,
        cp: 3,
        picture: "https://www.shinyhunters.com/images/regular/96.gif",
        types: ["Psy"],
        created: new Date()
    }, {
        id: 97,
        name: "Hypnomade",
        hp: 25,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/97.gif",
        types: ["Psy"],
        created: new Date()
    },
    {
        id: 98,
        name: "Krabby",
        hp: 20,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/98.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 99,
        name: "Krabboss",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/99.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 100,
        name: "Voltorbe",
        hp: 20,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/100.gif",
        types: ["Électrik"],
        created: new Date()
    },
    {
        id: 101,
        name: "Électrode",
        hp: 25,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/101.gif",
        types: ["Électrik"],
        created: new Date()
    },
    {
        id: 102,
        name: "Noeunoeuf",
        hp: 20,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/102.gif",
        types: ["Plante", "Psy"],
        created: new Date()
    },
    {
        id: 103,
        name: "Noadkoko",
        hp: 25,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/103.gif",
        types: ["Plante", "Psy"],
        created: new Date()
    },
    {
        id: 104,
        name: "Osselait",
        hp: 20,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/104.gif",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 105,
        name: "Ossatueur",
        hp: 25,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/105.gif",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 106,
        name: "Kicklee",
        hp: 35,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/106.gif",
        types: ["Combat"],
        created: new Date()
    },{
        id: 107,
        name: "Tygnon",
        hp: 35,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/107.gif",
        types: ["Combat"],
        created: new Date()
    },
    {
        id: 108,
        name: "Excelangue",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/108.gif",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 109,
        name: "Smogo",
        hp: 35,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/109.gif",
        types: [ "Poison"],
        created: new Date()
    },
    {
        id: 110,
        name: "Smogogo",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/110.gif",
        types: ["Poison"],
        created: new Date()
    },
    {
        id: 111,
        name: "Rhinocorne",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/111.gif",
        types: ["Sol", "Roche"],
        created: new Date()
    },
    {
        id: 112,
        name: "Rhinoféros",
        hp: 25,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/112.gif",
        types: ["Sol", "Roche"],
        created: new Date()
    },
    {
        id: 113,
        name: "Leveinard",
        hp: 30,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/133.gif",
        types: ["Poison", "Fée"],
        created: new Date()
    },
    {
        id: 114,
        name: "Saquedeneu",
        hp: 20,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/114.gif",
        types: ["Plante"],
        created: new Date()
    },
    {
        id: 115,
        name: "Kangourex",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/115.gif",
        types: ["Normal"],
        created: new Date()
    },
    {
        id: 116,
        name: "Hypotrempe",
        hp: 20,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/116.gif",
        types: ["Eau", "Dragon"],
        created: new Date()
    },
    {
        id: 117,
        name: "Hypocéan",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/117.gif",
        types: ["Eau", "Dragon"],
        created: new Date()
    },
    {
        id: 118,
        name: "Poissirène",
        hp: 25,
        cp: 5,
        picture: "https://www.shinyhunters.com/images/regular/118.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 119,
        name: "Poissoroy",
        hp: 35,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/119.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 120,
        name: "Stari",
        hp: 20,
        cp: 4,
        picture: "https://www.shinyhunters.com/images/regular/120.gif",
        types: ["Eau"],
        created: new Date()
    },
    {
        id: 121,
        name: "Staross",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/121.gif",
        types: ["Eau", "Psy"],
        created: new Date()
    },
    {
        id: 122,
        name: "M. Mime",
        hp: 40,
        cp: 8,
        picture: "https://www.shinyhunters.com/images/regular/122.gif",
        types: ["Psy", "Fée"],
        created: new Date()
    },
    {
        id: 123,
        name: "Insécateur",
        hp: 35,
        cp: 7,
        picture: "https://www.shinyhunters.com/images/regular/123.gif",
        types: ["Insecte", "Vol"],
        created: new Date()
    },
    {
        id: 124,
        name: "Lippoutou",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/124.gif",
        types: ["Glace", "Psy"],
        created: new Date()
    },
    {
        id: 125,
        name: "Élektek",
        hp: 30,
        cp: 6,
        picture: "https://www.shinyhunters.com/images/regular/125.gif",
        types: ["Électrik"],
        created: new Date()
    },
    {
        id: 133,
        name: "Évoli",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/133.gif",
        types: ["Normal"],
        created: new Date()
    },
    
    {
        id: 134,
        name: "Aquali",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/134.gif",
        types: ["Eau"],
        created: new Date()
    },
    
    {
        id: 135,
        name: "Voltali",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/135.gif",
        types: ["Electrik"],
        created: new Date()
    },
    
    {
        id: 136,
        name: "Piroli",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/136.gif",
        types: ["Feu"],
        created: new Date()
    },
    {
        id: 144,
        name: "Artikodin",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/144.gif",
        types: ["Vol","Glace"],
        created: new Date()
    }, {
        id: 145,
        name: "Electhor",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/145.gif",
        types: ["Vol","Electrik"],
        created: new Date()
    }, {
        id: 146,
        name: "Sulfura",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/146.gif",
        types: ["Vol","Feu"],
        created: new Date()
    }, {
        id: 147,
        name: "Minidraco",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/147.gif",
        types: ["Dragon"],
        created: new Date()
    }, {
        id: 148,
        name: "Draco",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/148.gif",
        types: ["Dragon"],
        created: new Date()
    }, {
        id: 149,
        name: "Dracolosse",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/149.gif",
        types: ["Dragon"],
        created: new Date()
    }, {
        id: 150,
        name: "Mewtwo",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/150.gif",
        types: ["Psy"],
        created: new Date()
    }, {
        id: 151,
        name: "Mew",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/151.gif",
        types: ["Psy"],
        created: new Date()
    }, {
        id: 152,
        name: "Gernignon",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/152.gif",
        types: ["Plante"],
        created: new Date()
    }, {
        id: 153,
        name: "Macroniumn",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/153.gif",
        types: ["Plante"],
        created: new Date()
    }, {
        id: 154,
        name: "Meganium",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/154.gif",
        types: ["Plante"],
        created: new Date()
    },{
        id: 175,
        name: "Togepi",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/175.gif",
        types: ["Fée"],
        created: new Date()
    },
    
    
    {
        id: 196,
        name: "Mentali",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/196.gif",
        types: ["Psy"],
        created: new Date()
    },{
        id: 197,
        name: "Noctali",
        hp: 35,
        cp: 9,
        picture: "https://www.shinyhunters.com/images/regular/197.gif",
        types: ["Tenebres"],
        created: new Date()
    },
    

];
