import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListPokemonComponent } from './list-pokemon/list-pokemon.component';
import { DetailPokemonComponent } from './detail-pokemon/detail-pokemon.component';
import { BorderCardDirective } from './directives/border-card.directive';
import { ButtonDirective } from './directives/button.directive';
import { DetailDirective } from './directives/detail.directive';
import { TableauDirective } from './directives/tableau.directive';
import { ZoomImageDirective } from './directives/zoom-image.directive';
import { PokemonTypeColorPipe } from './pokemon-type-color.pipe';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '../page-not-found/page-not-found.component';
import { PokemonService } from './pokemon.service';
import { FormsModule } from '@angular/forms';
import { PokemonFormComponent } from './pokemon-form/pokemon-form.component';
import { EditPokemonComponent } from './edit-pokemon/edit-pokemon.component';
import { BlinkDirective } from './directives/blink.directive';
import { AddPokemonComponent } from './add-pokemon/add-pokemon.component';
import { SearchPokemonComponent } from './search-pokemon/search-pokemon.component';
import { LoaderComponent } from './loader/loader.component';
import { AuthGuard } from '../auth.guard';



const pokemonRoutes: Routes = [
  {path : 'edit/pokemon/:id', component : EditPokemonComponent,canActivate : [AuthGuard ]},
  {path : 'pokemon/add', component: AddPokemonComponent,canActivate : [AuthGuard ]},
  {path : 'pokemons', component: ListPokemonComponent,canActivate : [AuthGuard ]},
  {path : 'pokemon/:id', component: DetailPokemonComponent,canActivate : [AuthGuard ]},
 
    
  ];

@NgModule({
  declarations: [

DetailPokemonComponent,
BlinkDirective,
BorderCardDirective,
ButtonDirective,
DetailDirective,
TableauDirective,
ZoomImageDirective,
EditPokemonComponent,
ListPokemonComponent,
PokemonFormComponent,
PokemonTypeColorPipe,
AddPokemonComponent,
SearchPokemonComponent,
LoaderComponent,



  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule.forChild(pokemonRoutes)
  ],
  providers:[PokemonService]
})
export class PokemonModule { }
