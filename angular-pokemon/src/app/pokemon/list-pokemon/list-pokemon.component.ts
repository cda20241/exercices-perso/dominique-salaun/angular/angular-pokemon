import { Component,OnInit} from '@angular/core';
import {  Router } from '@angular/router';
import {Pokemon} from '../pokemon'
import { PokemonService } from '../pokemon.service';
@Component({
  selector: 'app-list-pokemon',
  templateUrl: './list-pokemon.component.html',
  styleUrl: './list-pokemon.component.scss'
})
export class ListPokemonComponent {
  pokemonList : Pokemon[]; 
  pokemon:Pokemon|undefined;

  constructor( 
    private router : Router,
    private pokemonService:PokemonService
    ){  }

ngOnInit(){
  this.pokemonService.getPokemonList()
  .subscribe(pokemonList => this.pokemonList = pokemonList);
}

  goToPokemonId(pokemon:Pokemon){
    this.router.navigate(['/pokemon',pokemon.id]);
  
  }
  goToAddPokemon(pokemon:Pokemon){
    this.router.navigate(['/pokemon/add']);
  }

}

  

