import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appButton]'
})
export class ButtonDirective {

  constructor(private el: ElementRef) {
    this.parDefaut();
  }

@HostListener('mouseenter') onMouseEnter() {
this.enFonction();

}

@HostListener('mouseleave') onMouseLeave() {
  this.parDefaut();
}

private parDefaut(){
this.setRadius('5px 5px 10px rgba(0, 0, 0, 0.6)');
this.setRotateAndZoom(0 , 1);
this.setColorButton('yellow');
this.setColorTxt('black')
this.setTailleTxt('auto')
this.setBorder('Blue');
this.setShadow('5px 5px 5px rgba(0, 0, 0, 0.6)');
this.setPoliceTxt('MV Boli');
this.setOutlineBox('2px solid red');
this.setOutlineTexte('2px 2px 2px red');
this.setTimeAnimation('transform 0.55s ease');

}

private enFonction(){
this.setRadius('10px 10px 10px rgba(0, 0, 0, 0.6)');
this.setRotateAndZoom(10, 1.5);
this.setColorButton('red');
this.setColorTxt('blue');
this.setTailleTxt('auto');
this.setBorder('purple');
this.setShadow('10px 10px 10px rgba(0, 0, 0, 0.6)');
this.setPoliceTxt('MV Boli');
this.setOutlineBox('2px solid blue');
this.setOutlineTexte('2px 2px 2px yellow');
this.setTimeAnimation('transform 0.75s ease');

}

private setTimeAnimation(time: string){
  this.el.nativeElement.style.transition = time;
  // use this.setTimeAnimation('transform 0.5s ease');
}

private setRotateAndZoom(rotation: number, scale: number) {
  this.el.nativeElement.style.transform = `rotate(${rotation}deg) scale(${scale})`;
}


private setRadius(angle: string) {
  let radius = angle;
  this.el.nativeElement.style.borderRadius = radius;
  //use  this.setRadius('10px 10px 10px rgba(0, 0, 0, 0.6)');
}

private setColorButton(color:string){

  this.el.nativeElement.style.background = color;
//use this.setColorButton('red');
}

private setColorTxt(color:string){
  this.el.nativeElement.style.color = color;
  //use this.setColorTxt('blue');
}

private setTailleTxt(Taille: string) {
  this.el.nativeElement.style.fontSize = Taille;
  //use this.setTailleTxt('25px');
}

private setBorder(color:string){
  let border = 'solid 4px ' + color;
  this.el.nativeElement.style.border = border;
// use this.setBorder('purple');

}

private setShadow(shadow: string) {
  this.el.nativeElement.style.boxShadow = shadow;
  // use this.setShadow('10px 10px 10px rgba(0, 0, 0, 0.6)');
}
private setPoliceTxt(police: string){
  this.el.nativeElement.style.fontFamily = police;
  // use this.setPoliceTxt('MV Boli');
}

private setOutlineBox(style:string){

  this.el.nativeElement.style.outline = style;
  //use this.setOutline('2px solid red');
}

private setOutlineTexte(style: string) {
  this.el.nativeElement.style.textShadow = style;
}



}
