import { Directive, ElementRef, HostListener, Input } from '@angular/core';


@Directive({
  selector: '[PokemonBorderCard]'
})
export class BorderCardDirective {
  @Input() cardSize: string;

  constructor(private el: ElementRef) {
    this.setDefaultStyles();
   
    
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.setHighlightedStyles();
   
  
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setDefaultStyles();
    
  }

  private setDefaultStyles() {
    this.setBorder('#f2f2f2');
    this.setRadius('40px');
    this.setShadow('none');
    this.setbackground('#885AC7')
    this.zoomRotateImage(0 ,0.5);
    this.el.nativeElement.style.transition = 'transform 0.5s ease';
    this.el.nativeElement.classList.remove('rotate-animation');
    this.setSize('360px', '150px');  // Définir la taille de la carte
    this.setAlignmentCenter("center");
  }

  private setHighlightedStyles() {
    this.setBorder('#2EFD00');
    this.setRadius('5px 5px 10px rgba(0, 0, 0, 0.6)');
    this.setShadow('10px 10px 10px rgba(0, 0, 0, 0.6)');
    this.setbackground('#7213F5')
    this.zoomRotateImage(3 , 0.6);
    this.el.nativeElement.classList.add('rotate-animation');
    this.setSize('360px', '150px'); // Définir la taille de la carte
    this.setAlignmentCenter("center");
  }



  /**
 * Définit l'alignement du texte au centre, gauche ou droite.
 * @param align - La valeur de l'alignement ('center', 'left' ou 'right').
 */
  private setAlignmentCenter(align: string) {
    this.el.nativeElement.style.textAlign = align;
  }
  private setBorder(color: string) {
    let border = 'solid 4px ' + color;
    this.el.nativeElement.style.border = border;
  }

  private setSize(widthSize: string, heightSize: string) {
    this.el.nativeElement.style.width = widthSize; // Définir la largeur de la carte
    this.el.nativeElement.style.height = heightSize; // Définir la hauteur de la carte
  }
  private setbackground(color: string) {
    let background =  color;
    this.el.nativeElement.style.background = background;
  }

  private setRadius(angle: string) {
    let radius = angle;
    this.el.nativeElement.style.borderRadius = radius;
  }

  private setShadow(shadow: string) {
    this.el.nativeElement.style.boxShadow = shadow;
  }

  private zoomRotateImage(rotation: number, scale: number) {
    this.el.nativeElement.style.transform = `rotate(${rotation}deg) scale(${scale})`;
  }


}



