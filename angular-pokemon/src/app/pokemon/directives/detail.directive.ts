import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appDetail]'
})
export class DetailDirective {

  constructor(private el: ElementRef) {
    this.setDefaultStyles();
  
    
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.setHighlightedStyles();
  
  
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setDefaultStyles();
    
  }

  private setDefaultStyles() {
    this.setBorder('#f2f2f2');
    this.setRadius('40px');
    this.setShadow('none');
    this.setbackground('#885AC7')
    this.zoomRotateImage(0, 0.5);
    this.el.nativeElement.style.transition = 'transform 0.5s ease';
    this.el.nativeElement.classList.remove('rotate-animation');
    this.setHeight("100%");
    this.setPadding("10px")
    this.setMargin("10px")
    this.setWidth("100%");
    // Définit la taille du texte de l'élément.
  this.setTailleTxt("25px");
  }

  private setHighlightedStyles() {
    this.setBorder('#2EFD00');
    this.setRadius('5px 5px 10px rgba(0, 0, 0, 0.6)');
    this.setShadow('10px 10px 10px rgba(0, 0, 0, 0.6)');
    this.setbackground('#7213F5')
    this.zoomRotateImage(0 , 0.5);
    this.el.nativeElement.classList.add('rotate-animation');
    this.setHeight("100%");
      this.setWidth("100%");// Définit la taille du texte de l'élément.
      this.setTailleTxt("25px");
    
  }

 /**
 * Définit la taille du texte de l'élément.
 * @param Taille - La taille du texte au format CSS ('25px', '2em', etc.).
 */
 private setTailleTxt(Taille: string) { this.el.nativeElement.style.fontSize = Taille;  //use this.setTailleTxt('25px');
}

  private setMargin(value: string) {
    this.el.nativeElement.style.margin = value;
    // use this.setMargin('10px');
  }


  private setPadding(value: string) {
    this.el.nativeElement.style.padding = value;
    // use this.setPadding('10px');
  }

  private setBorder(color: string) {
    let border = 'solid 4px ' + color;
    this.el.nativeElement.style.border = border;
  }

  private setbackground(color: string) {
    let background =  color;
    this.el.nativeElement.style.background = background;
  }

  private setRadius(angle: string) {
    let radius = angle;
    this.el.nativeElement.style.borderRadius = radius;
  }

  private setShadow(shadow: string) {
    this.el.nativeElement.style.boxShadow = shadow;
  }

  private zoomRotateImage(rotation: number, scale: number) {
    this.el.nativeElement.style.transform = `rotate(${rotation}deg) scale(${scale})`;
  }

  private setHeight(value: string) {
    this.el.nativeElement.style.height = value;
    // use this.setHeight('100px');
  }

  /**
 * Définit la largeur de l'élément.
 * @param value - La valeur de la largeur au format CSS ('200px', '50%', etc.).
 */
  private setWidth(value: string) {
    this.el.nativeElement.style.width = value;
    //use this.setWidth('200px');
  }

}



