import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appZoomImage]'
})
export class ZoomImageDirective {

  constructor(private el: ElementRef) {
    this.setDefaultStyles();
    this.zoomRotateImage(0, 0.8);
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.setHighlightedStyles();
  
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.setDefaultStyles();

  }

  private setDefaultStyles() {
    this.zoomRotateImage(0 ,0.8);
    this.el.nativeElement.classList.remove('rotate-animation');
    this.el.nativeElement.style.transition = 'transform 0.5s ease';
    this.setHeight("100%");
  

    this.setWidth("100%");
    
  }

  private setHighlightedStyles() {
    this.zoomRotateImage(3 , 1.1);
    this.el.nativeElement.classList.add('rotate-animation');
    this.el.nativeElement.style.transition = 'transform 0.5s ease';
    this.setHeight("100%");
  

    this.setWidth("100%");
    
  }


  private zoomRotateImage(rotation: number, scale: number) {
    this.el.nativeElement.style.transform = `rotate(${rotation}deg) scale(${scale})`;
  }

  /**
 * Définit la hauteur de l'élément.
 * @param value - La valeur de la hauteur au format CSS ('100px', '50%', etc.).
 */
  private setHeight(value: string) {
    this.el.nativeElement.style.height = value;
    // use this.setHeight('100px');
  }

  /**
 * Définit la largeur de l'élément.
 * @param value - La valeur de la largeur au format CSS ('200px', '50%', etc.).
 */
  private setWidth(value: string) {
    this.el.nativeElement.style.width = value;
    //use this.setWidth('200px');
  }
}