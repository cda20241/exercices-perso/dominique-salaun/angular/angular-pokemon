import { Directive, ElementRef, HostListener, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appTableau]'
})
export class TableauDirective {

  constructor(private el: ElementRef,private renderer: Renderer2) {
        this.parDefaut();
      }
    
    @HostListener('mouseenter') onMouseEnter() {
    this.enFonction();
    
    }
    
    @HostListener('mouseleave') onMouseLeave() {
      this.parDefaut();
    }
    
    private parDefaut(){
      this.setRadius('40px');
    this.setRotateAndZoom(0 , 1);
    this.setColorButton('');
    this.setColorTxt('')
    this.setTailleTxt('')
    this.setBorder('0');
    this.setShadow('');
    this.setPoliceTxt('MV Boli');
    this.setOutlineBox('');
    this.setOutlineTexte('');
    this.setTimeAnimation('');
    this.setAlignmentCenter('center');
    this.setMargin('10px');
    this.setPadding('10px');
     // espacer les colonne tableau
    this.setTableSpacing("20px")

    }
    
    private enFonction(){
      this.setRadius('40px');
    this.setRotateAndZoom(0, 1);
    this.setColorButton('');
    this.setColorTxt('');
    this.setTailleTxt('');
    this.setBorder('0');
    this.setShadow('');
    this.setPoliceTxt('MV Boli');
    this.setOutlineBox('');
    this.setOutlineTexte('');
    this.setTimeAnimation('');
    this.setAlignmentCenter('center');
    this.setMargin('10px');
    this.setPadding('10px');
     // espacer les colonne tableau
  this.setTableSpacing("20px")
  }
  private setPadding(value: string) {
    this.el.nativeElement.style.padding = value;
    // use this.setPadding('10px');
  }
  private setAlignmentCenter(align:string) {
    this.el.nativeElement.style.textAlign = align;
  }
  private setMargin(value: string) {
    this.el.nativeElement.style.margin = value;
    // use this.setMargin('10px');
  }  

    private setTimeAnimation(time: string){
      this.el.nativeElement.style.transition = time;
      // use this.setTimeAnimation('transform 0.5s ease');
    }
    
    private setRotateAndZoom(rotation: number, scale: number) {
      this.el.nativeElement.style.transform = `rotate(${rotation}deg) scale(${scale})`;
    }
    
    
    private setRadius(angle: string) {
      let radius = angle;
      this.el.nativeElement.style.borderRadius = radius;
      //use  this.setRadius('10px 10px 10px rgba(0, 0, 0, 0.6)');
    }
    
    private setColorButton(color:string){
    
      this.el.nativeElement.style.background = color;
    //use this.setColorButton('red');
    }
    
    private setColorTxt(color:string){
      this.el.nativeElement.style.color = color;
      //use this.setColorTxt('blue');
    }
    
    private setTailleTxt(Taille: string) {
      this.el.nativeElement.style.fontSize = Taille;
      //use this.setTailleTxt('25px');
    }
    
    private setBorder(color:string){
      let border = 'solid 4px ' + color;
      this.el.nativeElement.style.border = border;
    // use this.setBorder('purple');
    
    }
    
    private setShadow(shadow: string) {
      this.el.nativeElement.style.boxShadow = shadow;
      // use this.setShadow('10px 10px 10px rgba(0, 0, 0, 0.6)');
    }
    private setPoliceTxt(police: string){
      this.el.nativeElement.style.fontFamily = police;
      // use this.setPoliceTxt('MV Boli');
    }
    
    private setOutlineBox(style:string){
    
      this.el.nativeElement.style.outline = style;
      //use this.setOutline('2px solid red');
    }
    
    private setOutlineTexte(style: string) {
      this.el.nativeElement.style.textShadow = style;
    }
    
    private setTableSpacing(espace: string) {
      this.renderer.setStyle(this.el.nativeElement, 'border-collapse', 'separate');
      this.renderer.setStyle(this.el.nativeElement, 'border-spacing', espace);
    }

    }
    