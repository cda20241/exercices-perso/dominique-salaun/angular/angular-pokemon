import {
  Directive,
  ElementRef,
  HostListener,
  Renderer2
}
from '@angular/core';

@Directive({
  selector: '[appDirectiveModele]'
}) export class DirectiveModeleDirective {

   constructor(private el: ElementRef,private renderer: Renderer2) {  this.parDefaut(); 
  }

  /**
   * Applique les styles par défaut lors du survol de la souris.
   */
  @HostListener('mouseenter') onMouseEnter() {
    this.enFonction();

  }

  /**
   * Rétablit les styles par défaut lorsque la souris quitte l'élément.
   */
  @HostListener('mouseleave') onMouseLeave() {
    this.parDefaut();
  }

  /**
   * Applique les styles par défaut à l'élément.
   */
/**
 * Méthode pour définir les styles CSS par défaut d'un élément.
 * Chaque ligne de code correspond à un paramètre spécifique.
 */
private  parDefaut() {
  // Définit le rayon des coins de l'élément.
  this.setRadius("40px rgba(0, 0, 0, 0.6)");
  
  // Réinitialise la rotation et le zoom de l'élément.
  this.setRotateAndZoom(0, 1);
  
  // Définit la couleur de fond de l'élément.
  this.setColorbackground("yellow");
  
  // Définit la couleur du texte de l'élément.
  this.setColorTxt("black");
  
  // Définit la taille du texte de l'élément.
  this.setTailleTxt("auto");
  
  // Définit la bordure de l'élément.
  this.setBorder("Blue");
  
  // Définit l'ombre de l'élément.
  this.setShadow("5px 5px 5px rgba(0, 0, 0, 0.6)");
  
  // Définit la police du texte de l'élément.
  this.setPoliceTxt("MV Boli");
  
  // Définit la bordure extérieure de l'élément.
  this.setOutlineBox("2px solid red");
  
  // Définit la bordure du texte de l'élément.
  this.setOutlineTexte("2px 2px 2px red");
  
  // Définit le temps d'animation de l'élément.
  this.setTimeAnimation("transform 0.55s ease");
  
  // Définit l'alignement horizontal et vertical du contenu de l'élément.
  this.setAlignmentCenter("center");
  
  // Définit la marge de l'élément.
  this.setMargin("0px");
  
  // Définit le rembourrage de l'élément.
  this.setPadding("0px");
  
  // Définit la hauteur de l'élément.
  this.setHeight("100px");
  
  // Définit la largeur de l'élément.
  this.setWidth("200px");
  
  // Met le texte en italique.
  this.setItalic();
  
  // Met le texte en gras.
  this.setBold();
  
  // Souligne le texte.
  this.setUnderline();
  
  // Fait clignoter l'élément.
  this.setBlinkCSS("blink 1s infinite");
  
  // Définit le schéma de couleurs clair/sombre pour l'élément.
  this.setLightDarkColorScheme("light dark");
  
  // Définit le schéma de couleurs clair/sombre pour un élément enfant avec des styles CSS spécifiques.
  this.setLightDarkColorSchemeEnfant("light dark", "background-color: yellow;font-weight: bold;");
  // Définit une image de fond
  this.setBackGroundImage("URL ICI")

   // espacer les colonne tableau
  this.setTableSpacing("10px")
}


  /**
 * Méthode pour définir les styles CSS d'un élément en fonction de certains paramètres.
 * Chaque ligne de code correspond à un paramètre spécifique.
 */
private enFonction() {
    // Définit une image de fond
  this.setBackGroundImage("URL ICI")
  // Définit le rayon des coins de l'élément.
  this.setRadius("10px 10px 40px rgba(255, 0, 0, 0.6)");

    /*
    10px: Il s'agit du rayon horizontal pour le coin supérieur gauche.
    10px: Il s'agit du rayon vertical pour le coin supérieur gauche.
    40px: Il s'agit du rayon horizontal pour le coin inférieur droit.
    rgba(255, 0, 0, 0.6)
    use si besoin juste this.setRadius("40px"); 
    */
  
  // Applique une rotation et un zoom à l'élément.
  this.setRotateAndZoom(10, 1.5);
  
  // Définit la couleur de fond de l'élément.
  this.setColorbackground("red");
  
  // Définit la couleur du texte de l'élément.
  this.setColorTxt("blue");
  
  // Définit la taille du texte de l'élément.
  this.setTailleTxt("auto");
  
  // Définit la bordure de l'élément.
  this.setBorder("purple");
  
  // Définit l'ombre de l'élément.
  this.setShadow("10px 10px 10px rgba(0, 0, 0, 0.6)");
  
  // Définit la police du texte de l'élément.
  this.setPoliceTxt("MV Boli");
  
  // Définit la bordure extérieure de l'élément.
  this.setOutlineBox("2px solid blue");
  
  // Définit la bordure du texte de l'élément.
  this.setOutlineTexte("2px 2px 2px yellow");
  
  // Définit le temps d'animation de l'élément.
  this.setTimeAnimation("transform 0.75s ease");
  
  // Définit l'alignement horizontal et vertical du contenu de l'élément.
  this.setAlignmentCenter("center");
  
  // Définit la marge de l'élément.
  this.setMargin("0px");
  
  // Définit le rembourrage de l'élément.
  this.setPadding("0px");
  
  // Définit la hauteur de l'élément.
  this.setHeight("100px");
  
  // Définit la largeur de l'élément.
  this.setWidth("200px");
  
  // Met le texte en italique.
  this.setItalic();
  
  // Met le texte en gras.
  this.setBold();
  
  // Souligne le texte.
  this.setUnderline();
  
  // Fait clignoter l'élément.
  this.setBlinkCSS("blink 1s infinite");
  
  // Définit le schéma de couleurs clair/sombre pour l'élément.
  this.setLightDarkColorScheme("light dark");
  
  // Définit le schéma de couleurs clair/sombre pour un élément enfant avec des styles CSS spécifiques.
  this.setLightDarkColorSchemeEnfant("light dark", "background-color: yellow;font-weight: bold;");
  // espacer les colonne tableau
  this.setTableSpacing("10px")
}


  /**
 * Définit l'alignement du texte au centre, gauche ou droite.
 * @param align - La valeur de l'alignement ('center', 'left' ou 'right').
 */
  private setAlignmentCenter(align: string) {
    this.el.nativeElement.style.textAlign = align;
  }

  /**
 * Définit le temps de l'animation de transition.
 * @param time - La durée de l'animation au format CSS ('0.5s', '1s', etc.).
 */
  private setTimeAnimation(time: string) { this.el.nativeElement.style.transition = time;  // use this.setTimeAnimation('transform 0.5s ease');
  }

  /**
 * Applique une rotation et un zoom à l'élément.
 * @param rotation - L'angle de rotation en degrés.
 * @param scale - Le facteur d'échelle.
 */
  private setRotateAndZoom(rotation: number, scale: number) {
    this.el.nativeElement.style.transform = `rotate($ {
      rotation
    }
    deg) scale($ {
      scale
    })`;
  }

  /**
 * Définit le rayon des coins de l'élément.
 * @param angle - La valeur du rayon au format CSS ('20px', '10px 10px 10px rgba(0, 0, 0, 0.6)', etc.).
 */
  private setRadius(angle: string) { let radius = angle; this.el.nativeElement.style.borderRadius = radius;
    //use  this.setRadius('40px');
      //use  this.setRadius('10px 10px 10px rgba(0, 0, 0, 0.6)');

  }

  /**
 * Définit la couleur de fond de l'élément.
 * @param color - La couleur de fond au format CSS ('red', 'rgba(0, 0, 255, 0.5)', etc.).
 */
  private setColorbackground(color: string) {

     this.el.nativeElement.style.background = color;
    //use this.setColorButton('red');
  }

  /**
 * Définit la couleur du texte de l'élément.
 * @param color - La couleur du texte au format CSS ('blue', '#00ff00', etc.).
 */
  private setColorTxt(color: string) { this.el.nativeElement.style.color = color;  //use this.setColorTxt('blue');
  }

  /**
 * Définit la taille du texte de l'élément.
 * @param Taille - La taille du texte au format CSS ('25px', '2em', etc.).
 */
  private setTailleTxt(Taille: string) { this.el.nativeElement.style.fontSize = Taille;  //use this.setTailleTxt('25px');
  }

  /**
 * Définit la bordure de l'élément.
 * @param color - La couleur et le style de la bordure au format CSS ('solid 4px purple', 'dotted 2px #ff0000', etc.).
 */
  private setBorder(color: string) { let border = 'solid 4px ' + color; this.el.nativeElement.style.border = border;
    // use this.setBorder('purple');

  }

  /**
 * Définit l'ombre portée de l'élément.
 * @param shadow - La propriété de l'ombre portée au format CSS ('10px 10px 10px rgba(0, 0, 0, 0.6)', etc.).
 */
  private setShadow(shadow: string) { this.el.nativeElement.style.boxShadow = shadow;  // use this.setShadow('10px 10px 10px rgba(0, 0, 0, 0.6)');
  }

  /**
 * Définit la police de caractères du texte de l'élément.
 * @param police - Le nom de la police de caractères ('Arial', 'Times New Roman', etc.).
 */
  private setPoliceTxt(police: string) { this.el.nativeElement.style.fontFamily = police;  // use this.setPoliceTxt('MV Boli');
  }

  /**
 * Définit le style de contour de l'élément.
 * @param style - Le style de contour au format CSS ('2px solid red', 'dashed 1px blue', etc.).
 */
  private setOutlineBox(style: string) {

     this.el.nativeElement.style.outline = style;  //use this.setOutline('2px solid red');
  }

  /**
 * Définit le style de contour du texte de l'élément.
 * @param style - Le style de contour du texte au format CSS ('2px solid red', 'dashed 1px blue', etc.).
 */
  private setOutlineTexte(style: string) { this.el.nativeElement.style.textShadow = style;
  }

  /**
 * Définit la marge de l'élément.
 * @param value - La valeur de la marge au format CSS ('10px', '5%', etc.).
 */
  private setMargin(value: string) {
    this.el.nativeElement.style.margin = value;
    // use this.setMargin('10px');
  }

  /**
 * Définit le rembourrage de l'élément.
 * @param value - La valeur du rembourrage au format CSS ('10px', '5%', etc.).
 */
  private setPadding(value: string) {
    this.el.nativeElement.style.padding = value;
    // use this.setPadding('10px');
  }

  /**
 * Définit la hauteur de l'élément.
 * @param value - La valeur de la hauteur au format CSS ('100px', '50%', etc.).
 */
  private setHeight(value: string) {
    this.el.nativeElement.style.height = value;
    // use this.setHeight('100px');
  }

  /**
 * Définit la largeur de l'élément.
 * @param value - La valeur de la largeur au format CSS ('200px', '50%', etc.).
 */
  private setWidth(value: string) {
    this.el.nativeElement.style.width = value;
    //use this.setWidth('200px');
  }

  /**
 * Met le texte en italique.
 */
  private setItalic() {
    this.el.nativeElement.style.fontStyle = 'italic';
    // use     this.setItalic();

  }

  /**
 * Met le texte en gras.
 */
  private setBold() {
    this.el.nativeElement.style.fontWeight = 'bold';
    // use  this.setBold();

  }

  /**
 * Souligne le texte.
 */
  private setUnderline() {
    this.el.nativeElement.style.textDecoration = 'underline';
    // use this.setUnderline();

  }

  /**
 * Définit le schéma de couleur clair ou sombre.
 * @param theme - Le thème de couleur ('light', 'dark', etc.).
 */
  private setLightDarkColorScheme(theme: string) {
    this.el.nativeElement.style.colorScheme = theme;
    // use this.setLightDarkColorScheme('light dark');
  }

  /**
 * Applique un schéma de couleur clair ou sombre aux éléments enfants.
 * @param colorScheme - Le thème de couleur pour les éléments enfants ('light', 'dark', etc.).
 * @param colorSchemeEnfant - Les styles spécifiques à appliquer aux éléments enfants au format CSS.
 */
  private setLightDarkColorSchemeEnfant(colorScheme: string, colorSchemeEnfant: string) {
    // Utilisation de ng-deep pour appliquer les styles à des éléments enfants
    this.el.nativeElement.style.cssText = `color - scheme: colorScheme;
    ng - deep.child - element {
      colorSchemeEnfant
    }`;
    // use  this.setLightDarkColorSchemeEnfant('light dark',"background-color: yellow;font-weight: bold;")
    //  Styles spécifiques pour les éléments enfants 
    //exemple:
    //background-color: yellow;
    //font-weight: bold;

  }

  /**
 * Applique un clignotement à l'élément en utilisant des animations CSS.
 * @param clignotement - La propriété d'animation CSS pour le clignotement ('blink 1s infinite', 'blink 0.5s 5', etc.).
 */
  private setBlinkCSS(clignotement: string) {

    this.el.nativeElement.style.animation = clignotement; // Modifiez la durée ou d'autres propriétés selon vos besoins
    //use this.setBlink('blink 1s infinite');
    //use this.setBlink('blink 1s 5'); 

    //a mettre dans styles.scss
    //@keyframes blink {
    //0% { opacity: 1; }
    //50% { opacity: 0; }
    //100% { opacity: 1; }
  }


//a mettre dans balise <body>
  private setBackGroundImage(imageUrl: string){
    this.el.nativeElement.style.backgroundImage = `url('${imageUrl}')`;
    this.el.nativeElement.style.backgroundSize = 'cover';
    this.el.nativeElement.style.backgroundRepeat = 'no-repeat';
    this.el.nativeElement.style.backgroundPosition = 'center';
  }

// a mettre dans balise <Table>
  private setTableSpacing(espace: string) {
    this.renderer.setStyle(this.el.nativeElement, 'border-collapse', 'separate');
    this.renderer.setStyle(this.el.nativeElement, 'border-spacing', espace);
  }
}