import { Directive,ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appBlink]'
})
export class BlinkDirective {

   constructor(private el: ElementRef) {
    this.parDefaut(); 
  }

  @HostListener('mouseenter') onMouseEnter() {
    this.enFonction();

  }

  /**
   * Rétablit les styles par défaut lorsque la souris quitte l'élément.
   */
  @HostListener('mouseleave') onMouseLeave() {
    this.parDefaut();
  }

  private  parDefaut() {

    this.setBlinkCSS("blink 1s infinite");

  }

private enFonction(){
  this.setBlinkCSS("blink 1s infinite");

}


  /**
 * Applique un clignotement à l'élément en utilisant des animations CSS.
 * @param clignotement - La propriété d'animation CSS pour le clignotement ('blink 1s infinite', 'blink 0.5s 5', etc.).
 */
  private setBlinkCSS(clignotement: string) {

    this.el.nativeElement.style.animation = clignotement; // Modifiez la durée ou d'autres propriétés selon vos besoins
    //use this.setBlink('blink 1s infinite');
    //use this.setBlink('blink 1s 5'); 


}
}